import React, {useContext,useState,useEffect} from "react";
import {Link} from "react-router-dom";
import {ThemeContext} from "./ThemeContext"

const Nav = () => {

  const [theme, setTheme] = useContext(ThemeContext)
  const [btnText, setBtnText] = useState(theme);

  useEffect( () =>{
    switch(theme){
      case "dark": setBtnText('Light'); break;
      case "light": default: setBtnText('Dark'); break;
    }
  })

  const changeTheme = () =>{
    switch(theme){
      case "dark": setTheme('light'); break;
      case "light": default: setTheme('dark'); break;
    }
  }

  return (
    <>
      <nav className={theme}>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/tugas-9">Tugas 9</Link>
          </li>
          <li>
            <Link to="/tugas-10">Tugas 10</Link>
          </li>
          <li>
            <Link to="/tugas-11">Tugas 11</Link>
          </li>
          <li>
            <Link to="/tugas-12">Tugas 12</Link>
          </li>
          <li>
            <Link to="/tugas-13">Tugas 13</Link>
          </li>
          <li>
            <Link to="/tugas-14">Tugas 14</Link>
          </li>
          <li>
            <button value={theme} onClick={changeTheme}>{btnText}</button>
          </li>
        </ul>
      </nav>
    </>
  )
}

export default Nav