import React from "react";
import { Switch, Route } from "react-router";
import Tugas9 from '../Tugas-9/form.js';
import Tugas10 from '../Tugas-10/TabelHarga.js';
import Tugas11 from '../Tugas-11/Timer.js';
import Tugas12 from '../Tugas-12/TabelHarga.js';
import Tugas13 from '../Tugas-13/Axios.js';
import Tugas14 from '../Tugas-14/Buah.js';

const Routes = () => {

  return (
    <div className="table-wrap">
      <Switch>
        <Route exact path="/">
            <h2>Silakan Klik Navigasi</h2>
        </Route>
        <Route path="/tugas-9">
          <Tugas9/>
        </Route>
        <Route path="/tugas-10">
          <Tugas10/>
        </Route>
        <Route path="/tugas-11">
          <Tugas11/>
        </Route>
        <Route path="/tugas-12">
          <Tugas12/>
        </Route>
        <Route path="/tugas-13">
          <Tugas13/>
        </Route>
        <Route path="/tugas-14">
          <Tugas14/>
        </Route>
      </Switch>
    </div>
  );
};

export default Routes;