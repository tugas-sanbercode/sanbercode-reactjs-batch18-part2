import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Routes from './Routes.js';
import Nav from './Nav.js';
import {ThemeProvider} from "./ThemeContext"

const ShowPage = () =>{
    
    return (
        <>
            <html>
                <head>
                    <title>Tugas 15</title>
                </head>
                <body>
                    <Router>    
                        <ThemeProvider> 
                            <Nav/>
                        </ThemeProvider>   
                        <Routes/>
                    </Router>
                </body>
            </html>
        </>
    )
}

export default ShowPage