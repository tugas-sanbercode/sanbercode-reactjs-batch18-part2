import React from "react"
import {BuahProvider} from "./BuahContext"
import {FormProvider} from "./FormContext"
import BuahList from "./BuahList"
import BuahForm from "./BuahForm"

const Buah = () =>{
  return(
    <BuahProvider>
        <FormProvider>
            <BuahList/>
            <BuahForm/>
        </FormProvider>
    </BuahProvider>
  )
}

export default Buah