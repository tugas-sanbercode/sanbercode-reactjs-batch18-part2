import React, {useContext} from 'react';
import {BuahContext} from "./BuahContext";
import {FormContext} from './FormContext';
import axios from 'axios';

const BuahForm = () =>{

    const [, setBuah] = useContext(BuahContext);
    const [input, setInput] = useContext(FormContext);

    const addBuah = (buah) =>{
        var queryString = Object.keys(buah).map(key => key + '=' + buah[key]).join('&');
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, queryString)
        .then(res => {
            console.log(res)
        })
    }

    const editBuah = (id, buah) =>{

        var queryString = Object.keys(buah).map(key => key + '=' + buah[key]).join('&');

        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${id}`, queryString)
        .then(res => {
            console.log(res)
            setBuah(null)
        })

        console.log('edit buah')
    }
    const HandleChangeInput = (event) =>{

        let type = event.target.name
        let value = event.target.value

        console.log(input)
        setInput({...input, [type]:value})
    }

    const HandleSubmit = (event) =>{

        let id = input.id
        let nama = input.nama;
        let harga = input.harga;
        let berat = input.berat;

        //handle add list
        if(!id){
            addBuah({
                name: nama,
                price:harga,
                weight:berat
            })
            
            ResetForm()
        }
        // handle edit list
        else{
            editBuah(id,{
                name: nama,
                price: harga,
                weight: berat
            })

            ResetForm()
        }
        
        event.preventDefault()
    }

    const ResetForm = () =>{
        setInput({
            id: '',
            nama: '',
            harga: '',
            berat: '',
        })

        setBuah(null)
    }

    return (
        <>
            <div id="form">
                <form method="post" onSubmit={HandleSubmit}>
                <h2>Form Tambah Buah</h2>
                <div className="form-group">
                    <label htmlFor="#nama">Nama Buah</label>
                    <div className="input-wrap">
                    <input type="text" id="nama" name="nama" value={input.nama} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#harga">Harga</label>
                    <div className="input-wrap">
                    <input type="number" id="harga" name="harga" value={input.harga} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#berat">Berat (g)</label>
                    <div className="input-wrap">
                    <input type="number" id="berat" name="berat" value={input.berat} onChange={HandleChangeInput} required />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-wrap">
                    <input type="submit" value="Kirim" />
                    </div>
                </div>
                </form>
                </div>
        </>
    )
}

export default BuahForm