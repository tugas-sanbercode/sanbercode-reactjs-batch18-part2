import React, {useContext, useEffect} from "react"
import {BuahContext} from "./BuahContext"
import {FormContext} from "./FormContext"
import axios from 'axios';

const BuahList = () =>{

    const [buah, setBuah] = useContext(BuahContext)
    const [, setInput] = useContext(FormContext)

    useEffect( () => {
        getDaftarBuah()
    })

    const toKg = (berat) =>{        
        
        let kg = `${berat / 1000} kg`;

        return kg;
    }

    const getDaftarBuah = () => {

        if(buah===null){
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res => {
                setBuah(res.data.map( (el) =>{
                    return {
                        id:el.id,
                        nama: el.name,
                        harga: el.price,
                        berat:el.weight
                    }
                }))
            })
            console.log('get buah')
      }
    }

    const HandleRemove = (event) =>{
        
        let id = event.target.value

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then(res => {
            setBuah(null)
        })
    }    

    const HandleEdit = (event) =>{
        
        let id = event.target.value        
        let currBuah = getBuah(id)
        let nama = currBuah.nama
        let harga = currBuah.harga
        let berat = currBuah.berat
        
        setInput({
            id,
            nama,
            harga,
            berat
        })
    }

    const getBuah = (id) =>{
        let currBuah={}

        buah.forEach( (el)=>{
            if(el.id===Number(id)){
                currBuah = el;
            }
        })

        return currBuah
    }

    return(
        <>
            <h2>Tabel Harga Buah</h2>
            <table>
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>    
                </thead>
                <tbody>
                { buah!==null && buah.map( (el, index) =>{
                        return  <tr key={index}>
                                <td>{el.nama}</td>
                                <td>{el.harga}</td>
                                <td>{toKg(el.berat)}</td>
                                <td><button value={el.id} onClick={HandleEdit}>Edit</button> | <button value={el.id} onClick={HandleRemove}>Hapus</button></td>
                            </tr>                        
                        })
                }
                
                </tbody>
            </table>
        </>
    )
}

export default BuahList