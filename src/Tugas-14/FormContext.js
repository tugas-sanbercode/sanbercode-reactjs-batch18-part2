import React, {useState, createContext} from "react";

export const FormContext = createContext();

export const FormProvider = props => {
    
    const [input, setInput] = useState({
        id: '',
        nama: '',
        harga: '',
        berat: '',
    })

    return (
        <FormContext.Provider value={[input, setInput]}>
            {props.children}
        </FormContext.Provider>
    )
}