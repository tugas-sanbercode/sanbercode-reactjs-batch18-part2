import React from 'react'

class Form extends React.Component{
    render(){
        return <div id="form">
                <form method="post">
                <h2>Form Pembelian Buah</h2>
                <div className="form-group">
                    <label htmlFor="#name">Nama Pelanggan</label>
                    <div className="input-wrap">
                    <input type="text" id="name" name="name" required />
                    </div>
                </div>
                <div className="form-group">
                    <label>Daftar Item</label>
                    <div className="input-wrap">
                    <input type="checkbox" name="fruit[]" value="semangka" /> Semangka <br />
                    <input type="checkbox" name="fruit[]" value="jeruk" /> Jeruk <br />
                    <input type="checkbox" name="fruit[]" value="nanas" /> Nanas <br />
                    <input type="checkbox" name="fruit[]" value="salak" /> Salak <br />
                    <input type="checkbox" name="fruit[]" value="Anggur" /> Anggur <br />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-wrap">
                    <input type="submit" value="Kirim" />
                    </div>
                </div>
                </form>
            </div>;
    }
}

export default Form;