import React from "react";
import { Switch, Route } from "react-router";

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
          <h2>Home</h2>
      </Route>
      <Route path="/about">
         <h2>About</h2>
      </Route>
      <Route exact path="/dashboard">
         <h2>Dashboard</h2>
      </Route>
    </Switch>
  );
};

export default Routes;