import React, {Component} from 'react';

class TabelHarga extends Component{

    constructor(props){
        super(props)

        this.state={
            index: -1,
            inputNama:'',
            inputHarga:'',
            inputBerat:'',
            buah: [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ]
        }

        this.HandleChangeNama  = this.HandleChangeNama.bind(this)
        this.HandleChangeHarga = this.HandleChangeHarga.bind(this)
        this.HandleChangeBerat = this.HandleChangeBerat.bind(this)
        this.HandleSubmit = this.HandleSubmit.bind(this)
        this.HandleEdit = this.HandleEdit.bind(this)
        this.HandleRemove = this.HandleRemove.bind(this)
    }

    toKg(berat){        
        
        let kg = `${berat / 1000} kg`;

        return kg;
    }

    HandleChangeNama(event){
        this.setState({
            inputNama: event.target.value
        })
    }

    HandleChangeHarga(event){
        this.setState({
            inputHarga: event.target.value
        })
    }

    HandleChangeBerat(event){
        this.setState({
            inputBerat: event.target.value
        })
    }

    HandleSubmit(event){

        let index = this.state.index
        let nama = this.state.inputNama;
        let harga = this.state.inputHarga;
        let berat = this.state.inputBerat;

        //handle add list
        if(index === -1){
            this.setState({
                buah: [...this.state.buah, {nama, harga, berat}],
                inputNama: "",
                inputHarga: "",
                inputBerat: "",
                index: -1,
            })
        }
        // handle edit list
        else{

            let buah = this.state.buah

            buah[index].nama = this.state.inputNama
            buah[index].harga = this.state.inputHarga
            buah[index].berat = this.state.inputBerat

            this.setState({

                buah: buah,
                inputNama: "",
                inputHarga: "",
                inputBerat: "",
                index: -1,
            })
        }
        
        event.preventDefault()
    }

    HandleEdit(event){
        
        let index = event.target.value
        
        let inputNama = this.state.buah[index].nama
        let inputHarga = this.state.buah[index].harga
        let inputBerat = this.state.buah[index].berat
        
        this.setState({
            index: index,
            inputNama: inputNama,
            inputHarga: inputHarga,
            inputBerat: inputBerat,
        })
    }

    HandleRemove(event){
        
        let index = event.target.value
        this.state.buah.splice(index, 1);
        this.setState({
            buah: this.state.buah
        })
    }

    render(){
        return (
            <>
                <h2>Tabel Harga Buah</h2>
                <table>
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Aksi</th>
                        </tr>    
                    </thead>
                    <tbody>

                    {this.state.buah.map( (el, index) =>{
                        return  <tr key={index}>
                                    <td>{el.nama}</td>
                                    <td>{el.harga}</td>
                                    <td>{this.toKg(el.berat)}</td>
                                    <td><button value={index} onClick={this.HandleEdit}>Edit</button> | <button value={index} onClick={this.HandleRemove}>Hapus</button></td>
                                </tr>                        
                    })}
                    
                    </tbody>
                </table>

                <div id="form">
                    <form method="post" onSubmit={this.HandleSubmit}>
                    <h2>Form Tambah Buah</h2>
                    <div className="form-group">
                        <label htmlFor="#nama">Nama Buah</label>
                        <div className="input-wrap">
                        <input type="text" id="nama" name="nama" value={this.state.inputNama} onChange={this.HandleChangeNama} required />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="#harga">Harga</label>
                        <div className="input-wrap">
                        <input type="text" id="harga" name="harga" value={this.state.inputHarga} onChange={this.HandleChangeHarga} required />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="#berat">Berat (g)</label>
                        <div className="input-wrap">
                        <input type="text" id="berat" name="berat" value={this.state.inputBerat} onChange={this.HandleChangeBerat} required />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="input-wrap">
                        <input type="submit" value="Kirim" />
                        </div>
                    </div>
                    </form>
                 </div>
            </>
        )
    }
}

export default TabelHarga