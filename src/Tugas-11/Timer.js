import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)

    var currentdate = new Date(); 

    this.state = {
      time: 100,
      hour: currentdate.getHours(),
      minute: currentdate.getMinutes(),
      second: currentdate.getSeconds(),
    }
  }

  componentDidMount(){    

    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentDidUpdate(){
    if (this.state.time === 0){
      this.componentWillUnmount();
    }
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    
    let h = this.state.hour;
    let m = this.state.minute;
    let s = this.state.second;

    s = s + 1;

    if(s >= 60){
      s=0;
      m+=1;
    }
    if(m >= 60){
      m=0;
      h+=1;
    }
    if(h>=24){
      h=0;
    }
    
    this.setState({
      time: this.state.time - 1,
      hour: h,
      minute: m,
      second: s,
    });
  }


  render(){
    return(
      <>
        <h1 style={{textAlign: "center"}}>
        {this.state.hour}:{this.state.minute}:{this.state.second} &emsp; {this.state.time}
        </h1>
      </>
    )
  }
}

export default Timer