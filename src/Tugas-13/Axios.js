import React, {useState,useEffect} from 'react';
import axios from 'axios';

const Axios = () => {

    const [inputId, setId] = useState(-1);
    const [inputNama, setNama] = useState('');
    const [inputHarga, setHarga] = useState('');
    const [inputBerat, setBerat] = useState('');
    const [buah, setBuah] = useState([]);
    const [trigger, setTrigger] = useState(0);

    useEffect( () => {
        getDaftarBuah()
    })

    const getDaftarBuah = () => {

        if(!trigger){
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res => {
                let array = res.data.map( (el) =>{
                    console.log(el)
                    return {
                        id:el.id,
                        nama: el.name,
                        harga: el.price,
                        berat:el.weight
                    }
                })

                setBuah(array)
            })

            console.log('get buah')
            setTrigger(1)
      }
    }

    const getBuah = (id) =>{
        let currBuah={}

        buah.forEach( (el)=>{
            if(el.id===Number(id)){
                currBuah = el;
            }
        })

        return currBuah
    }

    const addBuah = (buah) =>{
        var queryString = Object.keys(buah).map(key => key + '=' + buah[key]).join('&');
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, queryString)
        .then(res => {
            console.log(res)
            setTrigger(0)
            getDaftarBuah()
        })
    }

    const editBuah = (id, buah) =>{

        var queryString = Object.keys(buah).map(key => key + '=' + buah[key]).join('&');

        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${id}`, queryString)
        .then(res => {
            console.log(res)
            setTrigger(0)
            getDaftarBuah()
        })

        console.log('edit buah')
    }

    const toKg = (berat) =>{        
        
        let kg = `${berat / 1000} kg`;

        return kg;
    }

    const HandleChangeNama = (event) =>{
        setNama(event.target.value)
    }

    const HandleChangeHarga = (event) =>{
        setHarga(event.target.value)
    }

    const HandleChangeBerat = (event) =>{
        setBerat(event.target.value)
    }

    const HandleSubmit = (event) =>{

        let id = inputId
        let nama = inputNama;
        let harga = inputHarga;
        let berat = inputBerat;

        //handle add list
        if(id === -1){
            addBuah({
                name: nama,
                price:harga,
                weight:berat
            })
            
            setNama("")
            setHarga("")
            setBerat("")
            setId(-1)
        }
        // handle edit list
        else{
            editBuah(id,{
                name: inputNama,
                price: inputHarga,
                weight: inputBerat
            })
            setNama("")
            setHarga("")
            setBerat("")
            setId(-1)
        }
        
        event.preventDefault()
    }

    const HandleEdit = (event) =>{
        
        let id = event.target.value        
        let currBuah = getBuah(id)
        let inputNama = currBuah.nama
        let inputHarga = currBuah.harga
        let inputBerat = currBuah.berat
        
        setNama(inputNama)
        setHarga(inputHarga)
        setBerat(inputBerat)
        setId(id)
    }

    const HandleRemove = (event) =>{
        
        let id = event.target.value

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then(res => {
            setTrigger(0)
            getDaftarBuah()
        })
    }

    return (
        <>
            <h2>Tabel Harga Buah</h2>
            <table>
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>    
                </thead>
                <tbody>

                {buah.map( (el, index) =>{
                    return  <tr key={el.id}>
                                <td>{el.nama}</td>
                                <td>{el.harga}</td>
                                <td>{toKg(el.berat)}</td>
                                <td><button value={el.id} onClick={HandleEdit}>Edit</button> | <button value={el.id} onClick={HandleRemove}>Hapus</button></td>
                            </tr>                        
                })}
                
                </tbody>
            </table>

            <div id="form">
                <form method="post" onSubmit={HandleSubmit}>
                <h2>Form Tambah Buah</h2>
                <div className="form-group">
                    <label htmlFor="#nama">Nama Buah</label>
                    <div className="input-wrap">
                    <input type="text" id="nama" name="nama" value={inputNama} onChange={HandleChangeNama} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#harga">Harga</label>
                    <div className="input-wrap">
                    <input type="text" id="harga" name="harga" value={inputHarga} onChange={HandleChangeHarga} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#berat">Berat (g)</label>
                    <div className="input-wrap">
                    <input type="text" id="berat" name="berat" value={inputBerat} onChange={HandleChangeBerat} required />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-wrap">
                    <input type="submit" value="Kirim" />
                    </div>
                </div>
                </form>
                </div>
        </>
    )
}

export default Axios