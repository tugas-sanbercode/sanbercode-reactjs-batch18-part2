import React, {useState} from 'react';

const Hook = () =>{

    const [index, setIndex] = useState(-1);
    const [inputNama, setNama] = useState('');
    const [inputHarga, setHarga] = useState('');
    const [inputBerat, setBerat] = useState('');
    const [buah, setBuah] = useState([
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
    ]);

    const toKg = (berat) =>{        
        
        let kg = `${berat / 1000} kg`;

        return kg;
    }

    const HandleChangeNama = (event) =>{
        setNama(event.target.value)
    }

    const HandleChangeHarga = (event) =>{
        setHarga(event.target.value)
    }

    const HandleChangeBerat = (event) =>{
        setBerat(event.target.value)
    }

    const HandleSubmit = (event) =>{

        let i = index
        let nama = inputNama;
        let harga = inputHarga;
        let berat = inputBerat;

        //handle add list
        if(i === -1){
            setBuah([...buah, {nama, harga, berat}])
            setNama("")
            setHarga("")
            setBerat("")
            setIndex(-1)
        }
        // handle edit list
        else{

            //let buah = buah

            buah[i].nama = inputNama
            buah[i].harga = inputHarga
            buah[i].berat = inputBerat

            setBuah(buah)
            setNama("")
            setHarga("")
            setBerat("")
            setIndex(-1)
        }
        
        event.preventDefault()
    }

    const HandleEdit = (event) =>{
        
        let i = event.target.value
        
        let inputNama = buah[i].nama
        let inputHarga = buah[i].harga
        let inputBerat = buah[i].berat
        
        setNama(inputNama)
        setHarga(inputHarga)
        setBerat(inputBerat)
        setIndex(index)
    }

    const HandleRemove = (event) =>{
        
        let i = event.target.value
        let newBuah = []

        for(let x=0; x<buah.length; x++){
            if(x!=i)
                newBuah.push(buah[x])
        }
        console.log(newBuah)
        setBuah(newBuah)
    }

    return (
        <>
            <h2>Tabel Harga Buah</h2>
            <table>
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>    
                </thead>
                <tbody>

                {buah.map( (el, index) =>{
                    return  <tr key={index}>
                                <td>{el.nama}</td>
                                <td>{el.harga}</td>
                                <td>{toKg(el.berat)}</td>
                                <td><button value={index} onClick={HandleEdit}>Edit</button> | <button value={index} onClick={HandleRemove}>Hapus</button></td>
                            </tr>                        
                })}
                
                </tbody>
            </table>

            <div id="form">
                <form method="post" onSubmit={HandleSubmit}>
                <h2>Form Tambah Buah</h2>
                <div className="form-group">
                    <label htmlFor="#nama">Nama Buah</label>
                    <div className="input-wrap">
                    <input type="text" id="nama" name="nama" value={inputNama} onChange={HandleChangeNama} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#harga">Harga</label>
                    <div className="input-wrap">
                    <input type="text" id="harga" name="harga" value={inputHarga} onChange={HandleChangeHarga} required />
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="#berat">Berat (g)</label>
                    <div className="input-wrap">
                    <input type="text" id="berat" name="berat" value={inputBerat} onChange={HandleChangeBerat} required />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-wrap">
                    <input type="submit" value="Kirim" />
                    </div>
                </div>
                </form>
                </div>
        </>
    )
}

export default Hook