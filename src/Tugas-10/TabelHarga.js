import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class TabelHarga extends React.Component{

    toKg(berat){        
        
        let kg = `${berat / 1000} kg`;

        return kg;
    }

    render(){
        return (
            <>
                <h2>Tabel Harga Buah</h2>
                <table>
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                        </tr>    
                    </thead>
                    <tbody>

                    
                    {dataHargaBuah.map( el=>{
                        return  <tr key={el.nama}>
                                    <td>{el.nama}</td>
                                    <td>{el.harga}</td>
                                    <td>{this.toKg(el.berat)}</td>
                                </tr>                        
                    })}
                    
                    </tbody>
                </table>
            </>
        )
    }
}

export default TabelHarga